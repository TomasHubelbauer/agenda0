# Carthage

[Guide](https://github.com/Carthage/Carthage#if-youre-building-for-os-x)

```sh
cd src/Agenda
carthage update --platform iOS
# Drag framework files from Carthage/Builds to Embedded Frameworks section in XCode
```
