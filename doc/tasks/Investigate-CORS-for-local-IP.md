# Investigate CORS for local IP

See if it will be possible to add a CORS header that would allow the computer IP origin to access `localhost`
on the phone.
