# Explore `URLProtocol`

[Docs](https://developer.apple.com/documentation/foundation/urlprotocol)

Could register a custom protocol and then use that for the WebSocket and the HTTP communication maybe?
Perhaps a LE certificate could be issued for it somehow so I don't need to use self-signed certs?
