let webSocket;
let peerConnection;
let dataChannel;

window.addEventListener('load', _ => {
    audit(0, 'window load');
    bootWebSocketConnection();
});

window.addEventListener('error', ({ message, filename, lineno, colno, error }) => {
    audit(3, 'window error', message, filename, lineno, colno, error);
});

// Web connection is used as a sanity check (to make sure the server is functioning).
async function bootWebConnection() {
    try {
        audit(0, 'web init');
        const response = await fetch('/agenda');
        const text = await response.text();
        audit(0, 'web success', text);
        bootWebSocketConnection();
    } catch (error) {
        audit(3, 'web failure', error.message, error);
    }
}

// WebSocket connection is used for real-time exchange of SDP for establishing the WebRTC connection.
function bootWebSocketConnection() {
    const url = new URL(window.location.href);
    url.protocol = 'ws:';

    audit(0, 'web socket init', url);
    try {
        webSocket = new WebSocket(url);
    } catch (error) {
        audit(3, 'web socket failure', error.message, error);
    }

    webSocket.addEventListener('open', _ => {
        audit(0, 'web socket open');
    });

    webSocket.addEventListener('message', async ({ data }) => {
        audit(0, 'web socket message', data);
        receiveWebSocketMessage(data);
    });

    webSocket.addEventListener('error', _ => {
        audit(0, 'web socket error');
    });

    webSocket.addEventListener('close', ({ code, reason, wasClean }) => {
        audit(0, 'web socket close', { code, reason, wasClean });
    });
}

// WebRTC connection is used for the application data exchange and synchronization between the peers.
function bootWebRTCConnection() {
    audit(0, 'web rtc init');
    try {
        peerConnection = new RTCPeerConnection({ iceServers: [ { urls: [ 'stun:stun.l.google.com:19302' ] } ] });
        audit(0, 'web rtc ice connection state', peerConnection.iceConnectionState);
        audit(0, 'web rtc ice gatheting state', peerConnection.iceGatheringState);
        audit(0, 'web rtc signaling state', peerConnection.signalingState);
    } catch (error) {
        audit(3, 'web rtc failure', error.message, error);
    }

    peerConnection.addEventListener('negotiationneeded', _ => {
        audit(0, 'web rtc negotiation needed');
    });

    peerConnection.addEventListener('icecandidate', ({ candidate }) => {
        if (candidate) {
            audit(0, 'web rtc answerer candidate');
            sendWebSocketMessage({ type: 'candidate', sdp: candidate.candidate, sdpMid: candidate.sdpMid, sdpMLineIndex: candidate.sdpMLineIndex });
        }
    });

    peerConnection.addEventListener('iceconnectionstatechange', _ => {
        audit(0, 'web rtc ice connection state', peerConnection.iceConnectionState);
    });

    peerConnection.addEventListener('icegatheringstatechange', _ => {
        audit(0, 'web rtc ice gatheting state', peerConnection.iceGatheringState);
    });

    peerConnection.addEventListener('signalingstatechange', _ => {
        audit(0, 'web rtc signaling state', peerConnection.signalingState);
    });

    peerConnection.addEventListener('datachannel', ({ channel }) => {
        dataChannel = channel;
    });

    dataChannel.addEventListener('message', ({ data, origin }) => {
        audit({ flow: 'webRTCDataChannel', type: 'message', data, origin });
        receiveDataChannelMessage(data);

        dataChannel.addEventListener('open', _ => {
            audit(0, 'data channel open');
            dataChannel.send("test")
        });
    
        dataChannel.addEventListener('message', ({ data, origin }) => {
            audit(0, 'data channel message', data, origin);
        });
    
        dataChannel.addEventListener('error', ({ message, filename, lineno, colno, error }) => {
            audit(3, 'data channel error', message, filename, lineno, colno, error);
        });
    
        dataChannel.addEventListener('close', _ => {
            audit(0, 'data channel close');
        });
    });
}

// TODO: Persist this in IndexedDB for debugging purposes.
function audit(level, message, ...data) {
    const auditUl = document.getElementById('auditUl');
    let messageLi = document.createElement('li');
    messageLi.textContent = message;
    if (data) {
        const dataUl = document.createElement('ul');
        messageLi.appendChild(dataUl);
        for (const datum of data) {
            const datumLi = document.createElement('li');
            datumLi.textContent = JSON.stringify(datum);
            dataUl.appendChild(datumLi);
        }
    }

    auditUl.insertAdjacentElement('afterbegin', messageLi);
}

function sendWebSocketMessage(message) {
    webSocket.send(JSON.stringify(message));
}

async function receiveWebSocketMessage(text) {
    const message = JSON.parse(text);
    switch (message.type) {
        case 'description': {
            try {
                bootWebRTCConnection();
                audit(0, 'web rtc offerer description');
                const description = new RTCSessionDescription({ type: 'offer', sdp: message.sdp });
                await peerConnection.setRemoteDescription(description);
            } catch (error) {
                audit(3, 'web rtc offerer description error', error.message, error);
            }

            try {
                audit(0, 'web rtc answerer description');
                const answer = await peerConnection.createAnswer();
                await peerConnection.setLocalDescription(answer);
                sendWebSocketMessage({ type: 'description', sdp: answer.sdp });
            } catch (error) {
                audit(3, 'web rtc answerer description error', error.message, error);
            }

            break;
        }
        case 'candidate': {
            try {
                audit(0, 'web rtc offerer candidate');
                const candidate = new RTCIceCandidate({ candidate: message.sdp, sdpMid: message.sdpMid, sdpMLineIndex: message.sdpMLineIndex });
                await peerConnection.addIceCandidate(candidate);
            } catch (error) {
                audit(3, 'web rtc offerer candidate error', error.message, error);
            }

            break;
        }
        default: audit(text);
    }
}

function sendWebSocketMessage(message) {
    audit({ flow: 'webSocket', type: 'outgoing', message });
    webSocket.send(JSON.stringify(message));
}

function sendDataChannelMessage(message) {
    audit({ flow: 'webRTCDataChannel', type: 'outgoing', message });
    dataChannel.send(JSON.stringify(message));
}

function receiveWebSocketMessage(message) {
    switch (message.type) {
        case 'candidate': {
            audit({ flow: 'webRTCConnection', type: 'answerer-candidate' });
            await peerConnection.addIceCandidate(new RTCIceCandidate({ candidate: message.candidate }));
            break;
        }
        case 'description': {
            audit({ flow: 'webRTCConnection', type: 'answerer-description' });
            await peerConnection.setRemoteDescription(new RTCSessionDescription({ sdp: message.sdp, type: 'answer' }));
            // TODO: Check if connection was established and send message with typed code from the phone.
            break;
        }
    }
}

function receiveDataChannelMessage(message) {
    // Application messages.
}
