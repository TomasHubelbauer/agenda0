# Agenda

> A mobile application serving a personal assistant web application to devices on the same network.

Agenda is a mobile application that launches a server on a local network serving a web application used to manage
personal data. The mobile application server is the main data storage and the web application clients synchronize
with it using a real-time connection.

## Running

| Prerequisite | Minimal Version | Optimal Version | Justification            |
|--------------|-----------------|-----------------|--------------------------|
| NodeJS       | 9.0?            | 9.4.3           | `--experimental-modules` |
| Yarn         | ?               | 1.3.2           | `npm` sucks              |

Build and run the mobile application in `src`, it will serve `www` automatically on the phone.

Run the web application on the computer using `yarn start` to connect to the phone.
This will allow you to iterate on it without rebuilding the mobile application for each change.
The live version of the web application is on [Bloggo](https://bloggo.herokuapp.com/agenda/www).

You won't be able to use this application on the phone due to CORS, so you need to mock the server.
[This issue is being worked on.](doc/tasks/Investigate-CORS-for-local-IP.md)

## Developing

I recommend opening the repository in XCode for Swift editing and in VS Code for JavaScript editing.
Deploy both the mobile application including the bundled web application changes from XCode.

In case a JavaScript error is encountered in a non-interactive flow of the web application,
attaching the Web Inspector to the iPhone Safari tab will show empty console and refreshing doesn't
seem to work (it probably crashes or stalls the mobile application), so its best to keep the web
application's `index.html` opened as a local file in Mac Safari to watch out for syntax error etc.

## Debugging

- On the phone, go to Settings -> Safari -> Advanced -> enable Web Inspector
- On the computer, go to Safari -> Develop -> iPhone -> Agenda

## Contributing

See [development plan](/doc/tasks) and [development log](doc/notes).
