// TODO: Why does `import express from express` fail with *SyntaxError: Unexpected identifier*?
const express = require('express');
const ip = require('ip');

const host = ip.address()
const port = 8000

const server = express()
server.use(express.static('www'))
server.listen(port, () => console.log(`Serving www on ${host}:${port}`))
