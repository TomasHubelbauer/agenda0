import UIKit
import Telegraph
import WebRTC

// TODO: Replace structs with enums with associated values and custom encode/decode logic

struct DescriptionServerMessage: Codable {
    var type: String
    var sdp: String
}

struct CandidateServerMessage: Codable {
    var type: String
    var sdp: String
    var sdpMid: String?
    var sdpMLineIndex: Int32
}

struct ClientMessage: Codable {
    var type: String
}

class ViewController: UIViewController {
    let server = Server()
    let peerConnectionFactory = RTCPeerConnectionFactory()
    let peerConnectionConstraints = RTCMediaConstraints(mandatoryConstraints: [:], optionalConstraints: [:])
    let peerConnectionConfiguration = RTCConfiguration()
    let dataChannelConfiguration = RTCDataChannelConfiguration()
    
    // TODO: Keep a double headed dictionary of web socket to peer connection and back
    // TODO: Same for data channel or does it have a reference to the peer connection?
    var webSocket: WebSocket!
    var peerConnection: RTCPeerConnection!
    var dataChannel: RTCDataChannel!

    override func viewDidLoad() {
        super.viewDidLoad()
        initializeTelegraph()
        initializeWebRTC()

        // TODO: Figure out how to use default object or add redirect route from `/` to `index.html`
        UIApplication.shared.open(URL(string: "http://localhost/index.html")!, options: [:], completionHandler: nil)
    }
    
    func initializeTelegraph() {
        server.route(.get, "/agenda") { (.ok, "Agenda") }
        server.serveDirectory(Bundle.main.url(forResource: "www", withExtension: nil)!, "/")
        server.webSocketDelegate = self
        try! server.start(onPort: 80)
    }
    
    func initializeWebRTC() {
        RTCInitializeSSL()
        peerConnectionConfiguration.iceServers = [RTCIceServer(urlStrings: ["stun:stun.l.google.com:19302"])]
    }
    
    func acceptAndOffer(webSocket: WebSocket) {
        self.webSocket = webSocket
        peerConnection = peerConnectionFactory.peerConnection(with: peerConnectionConfiguration, constraints: peerConnectionConstraints, delegate: self)
        
        dataChannel = peerConnection.dataChannel(forLabel: "agenda", configuration: dataChannelConfiguration)
        dataChannel.delegate = self
        
        peerConnection.offer(for: peerConnectionConstraints, completionHandler: { (sdp, err) in
            guard err == nil else {
                print("offer error")
                return
            }
            
            guard sdp != nil else {
                print("sdp error")
                return
            }
            
            self.peerConnection.setLocalDescription(sdp!, completionHandler: { (err) in
                guard err == nil else {
                    print("local sdp error")
                    return
                }
                
                self.sendWebSocketDescriptionMessage(DescriptionServerMessage(type: "description", sdp: sdp!.sdp))
            })
        })
    }
    
    func sendWebSocketDescriptionMessage(_ message: DescriptionServerMessage) {
        let payload = String(data: try! JSONEncoder().encode(message), encoding: .utf8)!
        webSocket.send(text: payload)
    }
    
    func sendWebSocketCandidateMessage(_ message: CandidateServerMessage) {
        let payload = String(data: try! JSONEncoder().encode(message), encoding: .utf8)!
        webSocket.send(text: payload)
    }
    
    func receiveWebSocketMessage(_ data: Data) {
        let message = try! JSONDecoder().decode(ClientMessage.self, from: data)
        switch (message.type) {
        case "description": do {
            let descriptionMessage = try! JSONDecoder().decode(DescriptionServerMessage.self, from: data)
            peerConnection.setRemoteDescription(RTCSessionDescription(type: .answer, sdp: descriptionMessage.sdp), completionHandler: { (err) in
                guard err == nil else {
                    print("local sdp error")
                    return
                }
                
                print("yall")
            })
        }
        default: print("Unknown message type \(message.type)")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

// MARK: Web socket
extension ViewController: ServerWebSocketDelegate {
    func server(_ server: Server, webSocketDidConnect webSocket: WebSocket, handshake: HTTPRequest) {
        print("web socket did connect")
        acceptAndOffer(webSocket: webSocket)
    }
    
    func server(_ server: Server, webSocketDidDisconnect webSocket: WebSocket, error: Error?) {
        print("web socket did disconnect")
    }
    
    func server(_ server: Server, webSocket: WebSocket, didReceiveMessage message: WebSocketMessage) {
        switch (message.opcode) {
        case .textFrame: do {
            //let text = String(data: message.payload.data!, encoding: .utf8)!
            //print("web socket did receive text frame message \(text)")
            receiveWebSocketMessage(message.payload.data!)
        }
        default: print("web socket did receive a non-text message")
        }
    }
    
    func server(_ server: Server, webSocket: WebSocket, didSendMessage message: WebSocketMessage) {
        switch (message.opcode) {
        case .textFrame: do {
            //let data = String(data: message.payload.data!, encoding: .utf8)!
            //print("web socket did send text frame message \(data)")
            }
        default: print("web socket did send a non-text message")
        }
    }
    
    func serverDidDisconnect(_ server: Server) {
        print("server did disconnect")
    }
}

// MARK: Peer connection
extension ViewController: RTCPeerConnectionDelegate {
    func peerConnection(_ peerConnection: RTCPeerConnection, didChange stateChanged: RTCSignalingState) {
        switch (stateChanged) {
        case .closed: print("peer connection signaling state is closed")
        case .haveLocalOffer: print("peer connection signaling state is has local offer")
        case .haveLocalPrAnswer: print("peer connection signaling state is has local pranswer")
        case .haveRemoteOffer: print("peer connection signaling state is has remote offer")
        case .haveRemotePrAnswer: print("peer connection signaling state is has remote pranswer")
        case .stable: print("peer connection signaling state is stable")
        }
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didAdd stream: RTCMediaStream) {
        print("peerConnection did add stream")
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didRemove stream: RTCMediaStream) {
        print("peerConnection did remove stream")
    }
    
    func peerConnectionShouldNegotiate(_ peerConnection: RTCPeerConnection) {
        print("peerConnection should negotiate")
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didChange newState: RTCIceConnectionState) {
        switch (newState) {
        case .checking: print("peer connection state is checking")
        case .closed: print("peer connection state is closed")
        case .completed: print("peer connection state is completed")
        case .connected: print("peer connection state is connected")
        case .count: print("peer connection state is count")
        case .disconnected: print("peer connection state is disconnected")
        case .failed: print("peer connection state is failed")
        case .new: print("peer connection state is new")
        }
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didChange newState: RTCIceGatheringState) {
        switch (newState) {
        case .complete: print("peer connection gathering state is complete")
        case .gathering: print("peer connection gathering state is gathering")
        case .new: print("peer connection gathering state is new")
        }
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didGenerate candidate: RTCIceCandidate) {
        print("peer connection did generate candidate")
        sendWebSocketCandidateMessage(CandidateServerMessage(type: "candidate", sdp: candidate.sdp, sdpMid: candidate.sdpMid, sdpMLineIndex: candidate.sdpMLineIndex))
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didRemove candidates: [RTCIceCandidate]) {
        print("peerConnection did remove candidate")
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didOpen dataChannel: RTCDataChannel) {
        print("peerConnection did open data channel")
    }
}

// MARK: Data channel
extension ViewController: RTCDataChannelDelegate {
    func dataChannelDidChangeState(_ dataChannel: RTCDataChannel) {
        print("data channel did change state")
        switch (dataChannel.readyState) {
        case .closed: print("data channel state is closed")
        case .closing: print("data channel state is closing")
        case .connecting: print("data channel state is connecting")
        case .open: print("data channel state is open")
        }
    }
    
    func dataChannel(_ dataChannel: RTCDataChannel, didReceiveMessageWith buffer: RTCDataBuffer) {
        let text = String(data: buffer.data, encoding: .utf8)!
        print("data channel did receive message '\(text)'")
        dataChannel.sendData(RTCDataBuffer(data: "Hey back".data(using: .utf8)!, isBinary: false))
    }
}
